(function ($) {

  Drupal.behaviors.alexcc = {

    attach: function (context, settings) {

      // All our js code here
      $(".open-login-button").on("click", function () {
        $("#block-user-login").slideToggle(); // .fadeToggle() // .slideToggle()
        console.log('test')
      });

      $(document).ready(function () {
        $(window).scroll(function () {
          navbarScroll();
        });
      });

      // Get the navbar
      var navbar = document.getElementById("block-nice_menus-1");
      var telephoneblock = document.getElementById("block-block-2");

      // Get the offset position of the navbar
      var sticky = navbar.offsetTop;

      // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
      function navbarScroll() {
        if (window.pageYOffset >= sticky) {
          navbar.classList.add("sticky");
          telephoneblock.classList.add("sticky");
        } else {
          navbar.classList.remove("sticky");
          telephoneblock.classList.remove("sticky");
        }
      }

      // end our js code

    }

  };
})(jQuery);
